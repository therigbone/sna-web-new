<?php include_once  ('db_connect.php');?>

<?php




//if ($_SESSION['user'])
//{
//	header('Location: index.php');
//
//}
//else
//{
//	header('Location: login.php');
//
//}

if ($_GET) {

    $updateID = $_GET['deleteid'];
    //$updateStatus = $_GET['action'];

    if ($updateStatus == "yes") {

        $updateSQL = "UPDATE categories SET deleted ='1' WHERE id = '$updateID'";
        $update_result = mysql_query($updateSQL);

    }




}

?>


<!DOCTYPE html>
<html>

<head>

    <title>Broadcastr | Dashboard</title>

    <meta name="viewport" content="width=device-width, initial-scale-1.0">

    <link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

    <!-- jQuery CSS -->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    <!--FontAwesome css-->

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" 	rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/u/dt/jq-2.2.3,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.12,b-1.2.1,b-colvis-1.2.1,b-flash-1.2.1,b-html5-1.2.1,b-print-1.2.1,fh-3.1.2,r-2.1.0,sc-1.4.2/datatables.min.css" />

    <?php include('css.php');?>
    <?php include('js.php');?>

    <style>

        td.details-control {
            background: url('images/details_open.png') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url('images/details_close.png') no-repeat center center;
        }

    </style>

</head>



<body>






<div class ="jumbotron" style="padding-left: 5%; padding-right: 5%; text-align: center; border-radius: 0">

    <center><h1>SNA Dashboard</h1></center>

    <hr width="65%">

    <br />

    <?php include 'nav.php' ?>

    <hr width="65%">

    <h2>Manage Categories</h2>


    <table id="categoryTable" class='display'>

        <thead>
        <tr>
            <th>

            </th>
            <th>
                <center>ID</center>
            </th>
            <th>
                <center>Category Name</center>
            </th>
            <th>
                <center>Edit</center>
            </th>

        </tr>
        </thead>
    </table>

</div>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Delete
            </div>
            <div class="modal-body">
                Are you sure you want to delete this category?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    function format ( d ) {
        // `d` is the original data object for the row
        return '<center><table>'+
            '<tr>'+
            '<td><button type="button">Add New Course for this Category</button></td>'+
            '<td><button type="button">Manage Existing Courses in this Category</button></td>'+
            '</tr>'+
            '</table></center>';
    }


    $(document).ready(function () {
        var table = $('#categoryTable').DataTable({

            dom: 'lBfrtip',
            "buttons": [
                'excel', 'pdf', 'csv' 
            ],
            "columns": [{
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },{
                "data": "id"
            },{
                "data": "category"
            },{
                "data": "id",
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" data-href="managecategory.php?deleteid='+data+'" data-toggle="modal" data-target="#confirm-delete">Delete</a>';
                }
            }],
            "order": [[1, 'asc']],
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "ajax": {
                url: 'process_category.php',
                type: 'POST'
            }
        });

        // Add event listener for opening and closing details
        $('#categoryTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );

    });

</script>

<script type="text/javascript">
    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
</script>

<script type="text/javascript" src="https://cdn.datatables.net/u/dt/jq-2.2.3,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.12,b-1.2.1,b-colvis-1.2.1,b-flash-1.2.1,b-html5-1.2.1,b-print-1.2.1,fh-3.1.2,r-2.1.0,sc-1.4.2/datatables.min.js"></script>



</body>



</html>