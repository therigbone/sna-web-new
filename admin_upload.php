<?php

include('db_connect.php');

if ($_POST) {

//Move file to correct folder

    $uploads_dir = 'uploads/'; //Directory to save the file that comes from client application.
    if ($_FILES["eventFile"]["error"] == UPLOAD_ERR_OK) {
        $tmp_name = $_FILES["eventFile"]["tmp_name"];
        $name = $_FILES["eventFile"]["name"];
        $Result = move_uploaded_file($tmp_name, "$uploads_dir/$name");

        echo $name;
    } else {
        echo "Error: Picture not Uploaded";
    }

    //get data from form submission and add to DB

    $eventName = $_POST['eventName'];
    $info = $_POST['eventInfo'];
    $venue = $_POST['eventVenue'];
    $date = $_POST['datepicker'];
    $type = $_POST['eventType'];

    $target_dir = "uploads/";
    $target_file = "http://events.applord-db.co.za/" . $target_dir . basename($_FILES["eventFile"]["name"]);

    $sql = "INSERT INTO applordd_motiv.events (name, date, venue, info, eventType, link, approved) VALUES ('$eventName', '$date', '$venue', '$info', '$type', '$target_file', 1)";

    $result = mysqli_query($connection, $sql);

    if ($result) {
        $success = true;
    } else {
        $fail = true;
    }


}

?>

<html>

<head>

    <title>Broadcastr | Dashboard</title>

    <meta name="viewport" content="width=device-width, initial-scale-1.0">

    <link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

    <!-- jQuery CSS -->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    <!--FontAwesome css-->

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" 	rel="stylesheet">

    <?php include('css.php');?>
    <?php include('js.php');?>

    <style>
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(images/Preloader_10.gif) center no-repeat #fff;
        }
    </style>





</head>

<script type="text/javascript">
    function ShowLoading(e) {
        var div = document.createElement('div');
        var img = document.createElement('img');
        img.src = '';
        div.innerHTML = "Loading...<br />";
        div.style.cssText = 'position: fixed; top: 0px; left: 0px; z-index: 9999; width: 100%; height: 100%; background: url(images/382.gif) center no-repeat  rgba(0, 0, 0, 0.71)';
        div.appendChild(img);
        document.body.appendChild(div);
        return true;
        // These 2 lines cancel form submission, so only use if needed.
        //window.event.cancelBubble = true;
        //e.stopPropagation();
    }
</script>

<script>
    //paste this code under the head tag or in a separate js file.
    // Wait for window load
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
</script>



<body>

<div class="se-pre-con"></div>




<div class ="jumbotron" style="padding-left: 5%; padding-right: 5%; text-align: center; border-radius: 0">


    <!-- TODO Loading progress bar and popup -->






    <center><h1>SNA Dashboard</h1></center>

    <hr width="65%">

    <br />

    <center><h4>What would you like to do?</h4></center>

    <br />


    <a href="#" class="btn btn-success">Manage Tutors</a>

    <a href="#" class="btn btn-success">Manage Categories</a>

    <a href="#" class="btn btn-success">Manage Corporate Bodies</a>

    <a href="index.php" class="btn btn-success">Approve Uploads</a>

    <a href="admin_upload.php" class="btn btn-warning">Upload File</a>



    <br />
    <br />

    <hr width="65%">


        <form action="admin.php" method="post" enctype="multipart/form-data" id="uploadForm" name="uploadForm" onsubmit="ShowLoading()">
            <div class="form-group">
                <label for="eventName">Event Name</label>
                <input type="text" class="form-control" id="eventName" name="eventName" placeholder="Event Name">
            </div>

            <div class="form-group">
                <label for="eventInfo">Event Info</label>
                <textarea class="form-control" idv="eventInfo" name="eventInfo" placeholder="Event Info" rows="4"></textarea>
            </div>

            <div class="form-group">
                <label for="eventVenue">Event Venue</label>
                <input type="text" class="form-control" id="eventVenue" name="eventVenue" placeholder="Event Venue">
            </div>


            <div class="form-group">
                <label for="datepicker">Event Date</label>
                <input type="text" class="form-control" id="datepicker" name="datepicker">
            </div>


            <div class="form-group">
                <label for="eventType">Event Type</label>
                <select class="form-control" id="eventType" name="eventType">
                    <option value="inspirational">Inspirational</option>
                    <option value="conference">Live Conference</option>
                    <option value="motivational">Motivational Speaking</option>
                    <option value="training">Training Seminar</option>
                </select>
            </div>

            <div class="form-group">
                <label for="eventFile">Select File</label>
                <center><input type="file" id="eventFile" name="eventFile"></center>
            </div>


            <input type="hidden" id="sent" name="sent" value="true" />
            <button type="submit" class="btn btn-success" id="btnSubmit" name="btnSubmit">Submit</button>
        </form>
        <br />


</div>

</body>



</html>

<script>
    $(document).ready(
        /* This is the function that will get executed after the DOM is fully loaded */
        function () {
            $("#datepicker").datepicker({
                changeMonth: true,//this option for allowing user to select month
                changeYear: true //this option for allowing user to select from year range
            });
        }

    )
    ;
</script>


