<?php include_once('db_connect.php'); ?>

<?php


//if ($_SESSION['user'])
//{
//	header('Location: index.php');
//
//}
//else
//{
//	header('Location: login.php');
//
//}
//
//if ($_GET) {
//
//    $updateID = $_GET['id'];
//    $updateStatus = $_GET['action'];
//
//
//    if ($updateStatus == "no") {
//
//        $updateSQL = "UPDATE users SET active = '0' WHERE id = '$updateID'";
//        $update_result = mysql_query($updateSQL);
//
//    }
//
//
//}
//
//?>


<!DOCTYPE html>
<html>

<head>

    <title>Broadcastr | Dashboard</title>

    <meta name="viewport" content="width=device-width, initial-scale-1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

    <!-- jQuery CSS -->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    <!--FontAwesome css-->

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/u/dt/jq-2.2.3,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.12,b-1.2.1,b-colvis-1.2.1,b-flash-1.2.1,b-html5-1.2.1,b-print-1.2.1,fh-3.1.2,r-2.1.0,sc-1.4.2/datatables.min.css" />

    <?php include('css.php'); ?>
    <?php include('js.php'); ?>

</head>


<body>


<div class="jumbotron" style="padding-left: 5%; padding-right: 5%; text-align: center; border-radius: 0">

    <center><h1>SNA Dashboard</h1></center>

    <hr width="65%">

    <br/>

    <?php include 'nav.php' ?>

    <hr width="65%">

    <h2>Manage Tutors</h2>


    <table id="tutors" class='table table-striped table-hover'>

        <thead>
            <tr>
                <th>
                    <center>ID</center>
                </th>
                <th>
                    <center>First Name</center>
                </th>
                <th>
                    <center>Last Name</center>
                </th>
                <th>
                    <center>Email</center>
                </th>
                <th>
                    <center>Profile</center>
                </th>

            </tr>
        </thead>
    </table>

   
</div>



<script type="text/javascript">
    $(document).ready(function () {
        $('#tutors').DataTable({
            dom: 'lBfrtip',
            "buttons": [
                'excel', 'pdf', 'csv' 
            ],
            "columns": [{
                "data": "id"
            }, {
                "data": "first_name"
            },{
                "data": "last_name"
            },{
                "data": "email"
            },{
                "data": "id",
                "render": function ( data, type, full, meta ) {
                    return '<a href="tutorprofile.php?id='+data+'">View Profile</a>';
                }
            }],
            "processing": true,
            "serverSide": true,
            "responsive": true,

            "ajax": {
                url: 'process.php',
                type: 'POST'
            }
        });

    });
</script>

<script type="text/javascript" src="https://cdn.datatables.net/u/dt/jq-2.2.3,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.12,b-1.2.1,b-colvis-1.2.1,b-flash-1.2.1,b-html5-1.2.1,b-print-1.2.1,fh-3.1.2,r-2.1.0,sc-1.4.2/datatables.min.js"></script>

</body>


</html>