<?php

//Check if form has been submitted and upload accordingly


include('db_connect.php');

if ($_POST) {

    $submitted = true;

//Move file to correct folder

    $uploads_dir = 'uploads/'; //Directory to save the file that comes from client application.
    if ($_FILES["eventFile"]["error"] == UPLOAD_ERR_OK) {
        $tmp_name = $_FILES["eventFile"]["tmp_name"];
        $name = $_FILES["eventFile"]["name"];
        $Result = move_uploaded_file($tmp_name, "$uploads_dir/$name");

        echo $name;
    } else {
        echo "Error: Picture not Uploaded";
    }

    //get data from form submission and add to DB

    $eventName = $_POST['eventName'];
    $info = $_POST['eventInfo'];
    $venue = $_POST['eventVenue'];
    $date = $_POST['datepicker'];
    $type = $_POST['eventType'];

    $target_dir = "uploads/";
    $target_file = "http://events.applord-db.co.za/" . $target_dir . basename($_FILES["eventFile"]["name"]);

    $sql = "INSERT INTO applordd_motiv.events (name, date, venue, info, eventType, link) VALUES ('$eventName', '$date', '$venue', '$info', '$type', '$target_file')";

    $result = mysql_query($sql, $connection);

    if ($result) {
        $success = true;
    } else {
        $fail = true;
    }


}

?>


<!DOCTYPE html>
<html>

<head>

    <title>Broadcastr | Dashboard</title>

    <meta name="viewport" content="width=device-width, initial-scale-1.0">

    <link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

    <!-- jQuery CSS -->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    <!--FontAwesome css-->

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" 	rel="stylesheet">

    <?php include('css.php');?>
    <?php include('js.php');?>

    <style>
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(images/Preloader_10.gif) center no-repeat #fff;
        }
    </style>





</head>

<script type="text/javascript">
    function ShowLoading(e) {
        var div = document.createElement('div');
        var img = document.createElement('img');
        img.src = '';
        div.innerHTML = "Loading...<br />";
        div.style.cssText = 'position: fixed; top: 0px; left: 0px; z-index: 9999; width: 100%; height: 100%; background: url(images/382.gif) center no-repeat  rgba(0, 0, 0, 0.71)';
        div.appendChild(img);
        document.body.appendChild(div);
        return true;
        // These 2 lines cancel form submission, so only use if needed.
        //window.event.cancelBubble = true;
        //e.stopPropagation();
    }
</script>

<script>
    //paste this code under the head tag or in a separate js file.
    // Wait for window load
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
</script>



<body>

<div class="se-pre-con"></div>




<div class ="jumbotron" style="padding-left: 5%; padding-right: 5%; text-align: center; border-radius: 0">


    <!-- TODO Loading progress bar and popup -->









    <?php

    if ($success == true) {
        echo '
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <strong>Thank You!</strong> Your upload will be moderated shortly.  You may safely close the browser and return to the Broadcastr App.

            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

    </div>';

    }

    if ($fail == true) {
        echo '
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <strong>Oops!</strong> Something went wrong.  Please try again.

            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

    </div>';
    }

    ?>




    <form action="dashboard.php" method="post" enctype="multipart/form-data" id="uploadForm" name="uploadForm" onsubmit="ShowLoading()">
        <div class="form-group">
            <label for="eventName">Event Name</label>
            <input type="text" class="form-control" id="eventName" name="eventName" placeholder="Event Name">
        </div>

        <div class="form-group">
            <label for="eventInfo">Event Info</label>
            <textarea class="form-control" idv="eventInfo" name="eventInfo" placeholder="Event Info" rows="4"></textarea>
        </div>

        <div class="form-group">
            <label for="eventVenue">Event Venue</label>
            <input type="text" class="form-control" id="eventVenue" name="eventVenue" placeholder="Event Venue">
        </div>


        <div class="form-group">
            <label for="datepicker">Event Date</label>
            <input type="text" class="form-control" id="datepicker" name="datepicker">
        </div>


        <div class="form-group">
            <label for="eventType">Event Type</label>
            <select class="form-control" id="eventType" name="eventType">
                <option value="inspirational">Inspirational</option>
                <option value="conference">Live Conference</option>
                <option value="motivational">Motivational Speaking</option>
                <option value="training">Training Seminar</option>
            </select>
        </div>

        <div class="form-group">
            <label for="eventFile">Select File</label>
            <center><input type="file" id="eventFile" name="eventFile"></center>
        </div>


        <input type="hidden" id="sent" name="sent" value="true" />
        <button type="submit" class="btn btn-success" id="btnSubmit" name="btnSubmit">Submit for Moderation</button>
    </form>
    <br />
    <a href="login.php" class="btn btn-default">Admin Login</a>

</div>

</body>



</html>

<script>
    $(document).ready(
        /* This is the function that will get executed after the DOM is fully loaded */
        function () {
            $("#datepicker").datepicker({
                changeMonth: true,//this option for allowing user to select month
                changeYear: true //this option for allowing user to select from year range
            });
        }

    )
    ;
</script>


