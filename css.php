<?php
// CSS File 




?>	
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	
	<!-- jQuery CSS -->
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	
	<!--FontAwesome css-->
	
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" 	rel="stylesheet">
	
	<style>
	html {
	  position: relative;
	  min-height: 100%;
	  width: 100%;
	  margin-bottom: 50px;
	}
	body {
		position: relative;
		margin-top: 70px;
	 	margin-bottom: 50px;
		width: 90%;
		margin-left: auto;
		margin-right: auto;
	  background-color: #444444;
	}
	
	hr { 
    display: block;
    margin-top: 0.5em;
    margin-bottom: 0.5em;
    margin-left: auto;
    margin-right: auto;
    border-style: inset;
    border-width: 1px;
}
	
	.panel-group {
		width: relative;
		margin-left: 7%;
		margin-right: 7%;
	}
	
	.footer {
	  position: fixed;
	  color: #D4D4D4;
	  bottom: 0;
	  left: 0;
	  width: 100%;
	  /* Set the fixed height of the footer here */
	  height: 50px;
	  background-color: #2A2A2A;
	}
	
	.navbar {
		position: fixed;
		left: 0;
		top: 0;
		width: 100%;
		margin: 0 0 1em 0;
		
		
	}
	
	.panel-body {
		overflow-x: scroll;
	}
	
	.h1 {
		text-shadow: 1px 1px 1px #000000;
	}
	
	.jumbotron {
		border-radius: 30px;
		box-shadow:  10px 10px 30px  #000000;
		background-color: #d1d1d1;
	}


	.nav-container {
		position: absolute;
		left: -50%;
		top: 0;
		background: #CD5334;
		height: 100%;
		width: 150px;
	}

	.nav-list {
		cursor: pointer;
		list-style-type: none;
		top: 50%;
		text-align: center;
		position: relative;
		margin: 0;
		padding: 0;
		-webkit-transform: translateY(-50%);
		-moz-transform: translateY(-50%);
		-ms-transform: translateY(-50%);
		-o-transform: translateY(-50%);
		transform: translateY(-50%);
	}

	.list-item {
		margin: 20px auto;
		border: 2px solid #fff;
		width: 50px;
		padding: 15px;
		-webkit-transition: all 1s ease;
		-moz-transition: all 1s ease;
		-ms-transition: all 1s ease;
		-o-transition: all 1s ease;
		transition: all 1s ease;
	}

	.list-item:hover {
		border: 2px solid #17BEBB;
		-webkit-transition: all 1s ease;
		-moz-transition: all 1s ease;
		-ms-transition: all 1s ease;
		-o-transition: all 1s ease;
		transition: all 1s ease;
	}

	@keyframes
	showNav { from {
		left: -100%;
	}

		to { left: 0; }
	}

	@-webkit-keyframes
	showNav { from {
		left: -100%;
	}

		to { left: 0; }
	}

	@-moz-keyframes
	showNav { from {
		left: -100%;
	}

		to { left: 0; }
	}

	@-o-keyframes
	showNav { from {
		left: -100%;
	}

		to { left: 0; }
	}

	.showNav {
		-webkit-animation: showNav .5s ease forwards;
		-moz-animation: showNav 1s ease forwards;
		-o-animation: showNav 1s ease forwards;
		animation: showNav .5s ease forwards;
	}

	@keyframes
	hideNav { from {
		left: 0;
	}

		to { left: -100%; }
	}

	@-webkit-keyframes
	hideNav { from {
		left: 0;
	}

		to { left: -100%; }
	}

	@-moz-keyframes
	hideNav { from {
		left: 0;
	}

		to { left: -100%; }
	}

	@-o-keyframes
	hideNav { from {
		left: 0;
	}

		to { left: -100%; }
	}

	.hideNav {
		-webkit-animation: hideNav 1s ease forwards;
		-moz-animation: hideNav 1s ease forwards;
		-o-animation: hideNav 1s ease forwards;
		animation: hideNav 1s ease forwards;
	}


	/* The switch - the box around the slider */
	.switch {
		position: relative;
		display: inline-block;
		width: 60px;
		height: 34px;
	}

	/* Hide default HTML checkbox */
	.switch input {display:none;}

	/* The slider */
	.slider {
		position: absolute;
		cursor: pointer;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		background-color: #ccc;
		-webkit-transition: .4s;
		transition: .4s;
	}

	.slider:before {
		position: absolute;
		content: "";
		height: 26px;
		width: 26px;
		left: 4px;
		bottom: 4px;
		background-color: white;
		-webkit-transition: .4s;
		transition: .4s;
	}

	input:checked + .slider {
		background-color: #2196F3;
	}

	input:focus + .slider {
		box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
		-webkit-transform: translateX(26px);
		-ms-transform: translateX(26px);
		transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
		border-radius: 34px;
	}

	.slider.round:before {
		border-radius: 50%;
	}


	</style>