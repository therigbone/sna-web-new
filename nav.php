<center><h4>What would you like to do?</h4></center>

<br />

<div class="btn-group">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Tutors <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li><a href="tutorapproval.php">Tutors Waiting for Approval</a></li>
        <li><a href="managetutors.php">Manage Tutors</a></li>
    </ul>
</div>

<div class="btn-group">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Users <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li><a href="manageusers.php">Manage Users</a></li>
    </ul>
</div>

<div class="btn-group">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Corporate Bodies <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li><a href="managecorporate.php">Manage Corporate Bodies</a></li>
    </ul>
</div>

<div class="btn-group">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Categories <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li><a href="managecategory.php">Manage Categories</a></li>
    </ul>
</div>

<div class="btn-group">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Uploads <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li><a href="uploadmanage.php">Approve Uploads</a></li>
        <li><a href="admin.php">Directly Upload a File</a></li>
    </ul>
</div>


<br />
<br />