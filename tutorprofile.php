<?php

//Check if form has been submitted and upload accordingly


include('db_connect.php');

if ($_GET) {

    $tutorID = $_GET['id'];

    $sql = "SELECT le.tutor AS tutor, le.category AS category, le.description AS description, le.link AS link, le.active AS active, c.category AS categoryName FROM lessons le JOIN categories c ON c.id = le.category WHERE tutor = '$tutorID'";
    $tutorSQL = "SELECT l.tutor AS tutor, l.category AS category, l.description AS description, l.link AS link, u.first_name AS firstName, u.last_name AS lastName, u.email AS email FROM lessons l JOIN users u ON l.tutor = u.id WHERE u.id = '$tutorID'";

    $resulta = mysql_query($sql, $connection);


    $tutorD = mysql_query($tutorSQL, $connection);
    $tutorDetails = mysql_fetch_assoc($tutorD);

    $button = "Save Profile";


}

?>


<!DOCTYPE html>
<html>

<head>

    <title>Broadcastr | Dashboard</title>

    <meta name="viewport" content="width=device-width, initial-scale-1.0">

    <link rel="stylesheet" 	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

    <!-- jQuery CSS -->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <!--FontAwesome css-->

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" 	rel="stylesheet">

    <?php include('css.php');?>
    <?php include('js.php');?>

    <style>
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(images/Preloader_10.gif) center no-repeat #fff;
        }
    </style>





</head>

<script type="text/javascript">
    function ShowLoading(e) {
        var div = document.createElement('div');
        var img = document.createElement('img');
        img.src = '';
        div.innerHTML = "Loading...<br />";
        div.style.cssText = 'position: fixed; top: 0px; left: 0px; z-index: 9999; width: 100%; height: 100%; background: url(images/382.gif) center no-repeat  rgba(0, 0, 0, 0.71)';
        div.appendChild(img);
        document.body.appendChild(div);
        return true;
        // These 2 lines cancel form submission, so only use if needed.
        //window.event.cancelBubble = true;
        //e.stopPropagation();
    }
</script>

<script>
    //paste this code under the head tag or in a separate js file.
    // Wait for window load
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
</script>



<body>

<div class="se-pre-con"></div>




<div class ="jumbotron" style="padding-left: 5%; padding-right: 5%; text-align: center; border-radius: 0">


    <center><h1>SNA Dashboard</h1></center>

    <hr width="65%">

    <br />

    <?php include('nav.php'); ?>

    <hr width="65%">

    <?php

    if (isset($_GET['tutorid'])) {

        $tutorsID = $_GET['tutorid'];


        //$sql = "SELECT le.tutor AS tutor, le.category AS category, le.description AS description, le.link AS link, le.active AS active, c.category AS categoryName FROM lessons le JOIN categories c ON c.id = le.category WHERE tutor = '$tutorID'";
        $tutorSQL = "SELECT * FROM users WHERE id = '$tutorsID'";

        //$resulta = mysql_query($sql, $connection);


        $tutorD = mysql_query($tutorSQL, $connection);
        $tutorDetails = mysql_fetch_assoc($tutorD);

        $button = "Approve";


    }

    ?>
    
    
    <h2>Tutor - <?php echo $tutorDetails['first_name'] . " " . $tutorDetails['last_name'];?></h2>



    <form action="tutorprofile.php" method="post" enctype="multipart/form-data" id="uploadForm" name="uploadForm" onsubmit="ShowLoading()" style="width: 70%; margin-left: auto; margin-right: auto;">
        <div class="form-group">
            <label for="eventName">First Name</label>
            <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" value="<?php if(isset($tutorDetails['first_name'])) {echo $tutorDetails['first_name']; } ?>">
        </div>

        <div class="form-group">
            <label for="eventInfo">Last Name</label>
            <input class="form-control" id="lastName" name="lastName" placeholder="Last Name" value="<?php if(isset($tutorDetails['last_name'])) {echo $tutorDetails['last_name']; } ?>">
        </div>

        <div class="form-group">
            <label for="eventVenue">Email Address</label>
            <input type="text" class="form-control" id="email" name="email" placeholder="Email Address" value="<?php if(isset($tutorDetails['email'])) {echo $tutorDetails['email']; } ?>">
        </div>

<!--        <div class="form-group">-->
<!--            <label for="eventVenue">Email Address</label>-->
<!--            <input type="text" class="form-control" id="eventVenue" name="eventVenue" placeholder="Event Venue">-->
<!--        </div>-->


<!--        <div class="form-group">-->
<!--            <label for="datepicker">Event Date</label>-->
<!--            <input type="text" class="form-control" id="datepicker" name="datepicker">-->
<!--        </div>-->


<!--        <div class="form-group">-->
<!--            <label for="eventFile">Select File</label>-->
<!--            <center><input type="file" id="eventFile" name="eventFile"></center>-->
<!--        </div>-->


        <input type="hidden" id="sent" name="sent" value="true" />
        <button type="submit" class="btn btn-success" id="btnSubmit" name="btnSubmit"><?php echo $button ?></button>
    </form>
    <br />
    <br />
    <hr width="65%">
    <br />
    <br />
    <h2>Tutors Uploads</h2>
    <!--Start of table for tutor categories dawg-->

    <table class="table table-condensed" style="width: 60%; margin-left: auto; margin-right: auto">
        <thead>
            <tr>
                <td style="font-style: italic"><b>Lesson Category</b></td>
                <td style="font-style: italic"><b>Lesson Description</b></td>
                <td  style="font-style: italic"><b>Link to File</b></td>
                <td  style="font-style: italic"><b>Status</b></td>
            </tr>
        </thead>
        <tbody>
                <?php while($result = mysql_fetch_assoc($resulta)) { ?>
            <tr>
                <td><?php echo $result['categoryName'];?></td>
                <td><?php echo $result['description'];?></td>
                <td><?php if($result['link'] != null) {echo "<a href='" . $result['link'] . "'>Click to View</a>";}?></td>
                <td><label class="switch">
                            <input type="checkbox" id="toggle_me" <?php if($result['active'] == '1') {echo "checked";} ?>>
                            <div class="slider round"></div>
                    </label>
                </td>
            </tr>
<?php } ?>
        </tbody>
    </table>

    <br />
    <br />

    <hr width="65%">




<!--    <a href="login.php" class="btn btn-default">Admin Login</a>-->

</div>

</body>



</html>

<script>
    $(document).ready(
        /* This is the function that will get executed after the DOM is fully loaded */
        function () {
            $("#datepicker").datepicker({
                changeMonth: true,//this option for allowing user to select month
                changeYear: true //this option for allowing user to select from year range
            });
        }

    )
    ;


    $.fn.toggleable = function(){this.on("click",function(){$(this).toggleClass("toggle-toggle").trigger("toggle",{on: $(this).hasClass("toggle-toggle")});});return this;};


    $(function(){

        $("#toggle_me").toggleable().on("toggle",function(event,toggle){
            if(toggle.on){
                //$(this).html("Toggle Me: On");

                //TODO Make Video Active/Inactive
            } else {
                //$(this).html("Toggle Me: Off");
            }
        });

    });
</script>


